package aksamedia.mark_image;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * @author Gabriele Mariotti (gabri.mariotti@gmail.com)
 */
public class ImgDrawingView extends ImageView {

    public static final float TOUCH_STROKE_WIDTH = 5;

    protected Path mPath;
    protected Paint mPaint;
    protected Paint mPaintFinal;
    protected Bitmap mBitmap;
    protected Canvas mCanvas;
    private ArrayList<Rect> rects=new ArrayList<Rect>();
    /**
     * Indicates if you are drawing
     */
    protected boolean isDrawing = false;

    /**
     * Indicates if the drawing is ended
     */


    protected float mStartX;
    protected float mStartY;

    protected float mx;
    protected float my;
    private float right;
    private float left;
    private float bottom;
    private float top;
    private ArrayList<class_rectangle> rect=new ArrayList<>();
    private boolean iskena;
    private float sx=0;
    private float sy=0;

    public ImgDrawingView(Context context) {
        super(context);
        init();
    }

    public ImgDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImgDrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public void zoom(){
        sx+=2;
        sy+=2;
        mCanvas.scale(sx,sy);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }
    public ArrayList<class_rectangle> getList(){
        return this.rect;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mBitmap, 0, 0, mPaint);

        if (isDrawing){
            onDrawRectangle(canvas);
        }
    }




    protected void init() {
        mPath = new Path();

        mPaint = new Paint(Paint.DITHER_FLAG);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(getContext().getResources().getColor(android.R.color.holo_blue_dark));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(TOUCH_STROKE_WIDTH);


        mPaintFinal = new Paint(Paint.DITHER_FLAG);
        mPaintFinal.setAntiAlias(true);
        mPaintFinal.setDither(true);
        mPaintFinal.setColor(getContext().getResources().getColor(android.R.color.holo_orange_dark));
        mPaintFinal.setStyle(Paint.Style.STROKE);
        mPaintFinal.setStrokeJoin(Paint.Join.ROUND);
        mPaintFinal.setStrokeCap(Paint.Cap.ROUND);
        mPaintFinal.setStrokeWidth(TOUCH_STROKE_WIDTH);
    }

    protected void reset() {
        mPath = new Path();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mx = event.getX();
        my = event.getY();
        onTouchEventRectangle(event);
        return true;
    }


    //------------------------------------------------------------------
    // Rectangle
    //------------------------------------------------------------------

    private void onDrawRectangle(Canvas canvas) {
        drawRectangle(canvas,mPaint);
    }

    private void onTouchEventRectangle(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                iskena=false;
                for(Rect rect : rects) {
                    if (rect.contains((int)mx, (int)my)) {
                        iskena=true;
                    }
                }
                if(iskena){
                    Intent intent = new Intent(getContext(), MainActivity1Open.class);
                    intent.putExtra("titik",getList());
                    getContext().startActivity(intent);
                }else {
                    isDrawing = true;
                    mStartX = mx;
                    mStartY = my;
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                if(!iskena) {
                    isDrawing = false;
                    mPaintFinal.setXfermode(null);
                    drawRectangle(mCanvas, mPaintFinal);
                    invalidate();
                    class_rectangle cls = new class_rectangle();
                    cls.setPaint(mPaintFinal);
                    cls.setStartPtx(left);
                    cls.setStartPty(top);
                    cls.setProjectedX(right);
                    cls.setProjectedY(bottom);
                    rect.add(cls);
                }
                break;
        }

    }

    public void restore(){
        if(rect.size()>0) {
            mPaintFinal.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            int current_index = rect.size() - 1;
            class_rectangle cls = rect.get(current_index);
            mCanvas.drawRect(cls.getStartPtx(), cls.getStartPty(), cls.getProjectedX(), cls.getProjectedY(), mPaintFinal);
            rect.remove(current_index);
            rects.remove(current_index);
            invalidate();
        }
    }
    public void set_rects(ArrayList<class_rectangle> rect){
        this.rect.addAll(rect);
        for(class_rectangle cls:this.rect){
            mPaintFinal.setXfermode(null);
            Log.e("tag1",String.valueOf(cls.getStartPtx())+"-"+String.valueOf(cls.getStartPty())+"-"+String.valueOf(cls.getProjectedX())+"-"+String.valueOf(cls.getProjectedY()));
            mCanvas.drawRect(cls.getStartPtx(), cls.getStartPty(), cls.getProjectedX(), cls.getProjectedY(), mPaintFinal);
            rects.add(new Rect((int)cls.getStartPtx(),(int) cls.getStartPty() , (int)cls.getProjectedX(),(int)cls.getProjectedY()));
        }
    }
    private void drawRectangle(Canvas canvas,Paint paint){
        right = mStartX > mx ? mStartX : mx;
        left = mStartX > mx ? mx : mStartX;
        bottom = mStartY > my ? mStartY : my;
        top = mStartY > my ? my : mStartY;
        canvas.drawRect(left, top , right, bottom, paint);
        rects.add(new Rect((int)left,(int) top , (int)right,(int) bottom));
    }



}