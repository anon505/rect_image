package aksamedia.mark_image;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.util.ArrayList;
public class MainActivity1Open extends Activity {

    ImgDrawingView imageResult;
    final int RQS_IMAGE1 = 1;

    Uri source;
    Bitmap bitmapMaster;
    private Button btn_zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1open);
        final ArrayList<class_rectangle> objects = (ArrayList<class_rectangle>)this.getIntent().getSerializableExtra("titik");
        Log.e("size",String.valueOf(objects.size()));
        imageResult = (ImgDrawingView) findViewById(R.id.result);
        btn_zoom=(Button) findViewById(R.id.btn_zoom);
        btn_zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        Picasso.with(this).load("https://pbs.twimg.com/media/BjYdHpDCQAA6v72.jpg").into(imageResult, new Callback() {
            @Override
            public void onSuccess() {
                imageResult.post(new Runnable() {
                    @Override
                    public void run() {
                        setRects(objects);
                    }
                });

            }

            @Override
            public void onError() {

            }
        });


    }
    public void setRects(ArrayList<class_rectangle> list){
        imageResult.set_rects(list);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap tempBitmap;

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case RQS_IMAGE1:
                    source = data.getData();

                    try {
                        //tempBitmap is Immutable bitmap,
                        //cannot be passed to Canvas constructor
                        tempBitmap = BitmapFactory.decodeStream(
                                getContentResolver().openInputStream(source));

                        Config config;
                        if(tempBitmap.getConfig() != null){
                            config = tempBitmap.getConfig();
                        }else{
                            config = Config.ARGB_8888;
                        }

                        //bitmapMaster is Mutable bitmap
                        bitmapMaster = Bitmap.createBitmap(
                                tempBitmap.getWidth(),
                                tempBitmap.getHeight(),
                                config);


                        imageResult.setImageBitmap(bitmapMaster);


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    break;
            }
        }
    }

}
