package aksamedia.mark_image;

import android.app.Activity;
import android.os.Bundle;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MainActivity1 extends Activity {
    private ImgDrawingView imgv;
    ArrayList<class_rectangle> objects;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        objects=new ArrayList<>();
        class_rectangle cls=new class_rectangle();
        cls.setStartPtx((float)100.19531);
        cls.setStartPty((float)86.143555);
        cls.setProjectedX((float)156.21556);
        cls.setProjectedY((float)157.21289);

        class_rectangle cls1=new class_rectangle();
        cls1.setStartPtx((float)318.64746);
        cls1.setStartPty((float)84.1416);
        cls1.setProjectedX((float)386.89655);
        cls1.setProjectedY((float)140.19629);
        objects.add(cls);
        objects.add(cls1);
        imgv=(ImgDrawingView) findViewById(R.id.result);
        Picasso.with(this).load("https://pbs.twimg.com/media/BjYdHpDCQAA6v72.jpg").into(imgv, new Callback() {
            @Override
            public void onSuccess() {
                imgv.post(new Runnable() {
                    @Override
                    public void run() {
                        setRects(objects);
                    }
                });
            }

            @Override
            public void onError() {

            }
        });
    }
    public void setRects(ArrayList<class_rectangle> list){
        imgv.set_rects(list);
    }
}
