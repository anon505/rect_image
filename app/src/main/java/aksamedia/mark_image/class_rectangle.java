package aksamedia.mark_image;

import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by macbookproretina13 on 4/15/17.
 */

public class class_rectangle implements Parcelable {
    float startPtx, startPty, projectedX, projectedY;
    Paint paint;
    /*public class_rectangle(float startPtx, float startPty,float projectedX,float projectedY,Paint paint){
        this.startPtx=startPtx;
        this.startPty=startPty;
        this.projectedX=projectedX;
        this.projectedY=projectedY;
        this.paint=paint;
    }*/
    public class_rectangle(){
    }

    protected class_rectangle(Parcel in) {
        startPtx = in.readFloat();
        startPty = in.readFloat();
        projectedX = in.readFloat();
        projectedY = in.readFloat();
    }

    public static final Creator<class_rectangle> CREATOR = new Creator<class_rectangle>() {
        @Override
        public class_rectangle createFromParcel(Parcel in) {
            return new class_rectangle(in);
        }

        @Override
        public class_rectangle[] newArray(int size) {
            return new class_rectangle[size];
        }
    };

    public float getProjectedX() {
        return projectedX;
    }

    public float getProjectedY() {
        return projectedY;
    }

    public float getStartPtx() {
        return startPtx;
    }

    public float getStartPty() {
        return startPty;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    public void setProjectedX(float projectedX) {
        this.projectedX = projectedX;
    }

    public void setProjectedY(float projectedY) {
        this.projectedY = projectedY;
    }

    public void setStartPtx(float startPtx) {
        this.startPtx = startPtx;
    }

    public void setStartPty(float startPty) {
        this.startPty = startPty;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(startPtx);
        dest.writeFloat(startPty);
        dest.writeFloat(projectedX);
        dest.writeFloat(projectedY);
    }
}
